import React from 'react';
//import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';
import Game from './Game';
import Setup from './Setup';

function App() {
  return (
    <BrowserRouter>
      <main>
        <nav>
          <ul>
            <li><a href="/game">Play</a></li>
            <li><a href="/setup">Prepare board</a></li>
          </ul>
        </nav>
        <Route path="/game" exact component={Game} />
        <Route path="/setup" exact component={Setup} />
      </main>
    </BrowserRouter>
  );
}

export default App;
