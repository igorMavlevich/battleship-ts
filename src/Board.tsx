import React from 'react';

function Cell(props: { onClick: React.MouseEventHandler<HTMLButtonElement> | undefined; value: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }) {
    return (
        <button className="cell" onClick={props.onClick}>
            {props.value}
        </button>
    );
}

interface BoardState {
    cells: Array<number | null>,
    isHumanNext: boolean,
    gameOn: boolean,
}

class Board extends React.Component<{}, BoardState>{
    constructor(props: Readonly<{}>) {
        super(props);
        this.state = {
            cells: Array(100).fill(null),
            isHumanNext: true,
            gameOn: false,
        };
    }

    renderRow(n: number) {
        var i: number = 0;
        var cells: JSX.Element[] = [];
        cells.push(<button>{n+1}</button>);
        for (i; i < 10; i++) {
            cells.push(this.renderCell(i+n*10));
        }

        return (
            <div className="board-row">
                {cells}
            </div>
        );
    }

    renderCell(i: number) {
        return(<Cell
        value = {this.state.cells[i]}
        onClick = {() => this.handleClick(i)}
        />);
    }

    render() {
        var i: number = 0;
        var rows: JSX.Element[] = [];

        for (i; i<10; i++){
            rows.push(this.renderRow(i));
        }

        return (<div className="board">
             <div className="board-row">
             <button></button>
            <button>A</button>
            <button>B</button>
            <button>C</button>
            <button>D</button>
            <button>E</button>
            <button>F</button>
            <button>G</button>
            <button>H</button>
            <button>I</button>
            <button>J</button>
            </div>
            {rows}
        </div>);
    }

    handleClick(i: number) {
        var ser = JSON.stringify(this.state);
        console.log(ser);
        // throw new Error('Method not implemented.');
    }
}

export default Board;